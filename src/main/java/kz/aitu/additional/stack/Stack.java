package kz.aitu.additional.stack;

public class Stack<T> {

    private Node top;
    private int size;

    public Stack() {
        this.size = 0;
        this.top = null;
    }

    public void push(T data) {
        Node node = new Node<T>(data);

        if(top == null) top = node;
        else {
            node.setNext(top);
            top = node;
        }
        size++;
    }

    public T getTop() {
        if(top == null) return null;
        return (T)top.getData();
    }

    public T pop() {
        Node node = top;

        if(top == null) return null;

        top = top.getNext();

        size--;
        return (T)node.getData();
    }

    public boolean empty() {
        return top == null;
    }

    public int size() {
        return this.size;
    }

    public void print() {
        Node current = top;

        while(current != null) {
            System.out.print(current.getData() + " ");
            current = current.getNext();
        }
        System.out.println();
    }
}
