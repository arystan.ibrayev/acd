package kz.aitu.additional.hashtable;

import com.sun.org.apache.xpath.internal.objects.XNodeSet;
import org.omg.CORBA.Current;

public class HashTable {

    private Node table[];
    private int size;

    public HashTable(int size) {
        table = new Node[size];
        this.size = size;
    }

    public void set(String key, String value) {
        int index = key.hashCode() % size;

        /*
        1)
        2)этирировать ноду пока мы не найдем наш кей.
         если нашли то поменяйте ему валью.
         3)еcли мы не нашли кей то создать новую ноду и
         добавить его в конец списка.



        */

        if (table[index] == null) {
            table[index] = new Node(key, value);
        } else {
            for (Node current = table[index]; current != null; current = current.getNext()) {
                if (current.getKey() == key) current.setValue(value);
                else if (current.getNext() == null) {
                    current.setNext(new Node(key, value));
                }
            }
        }


    }


    public String get(String key) {
/*
1) Найти индекс
2) Найти нужный нод с подходящим кием который нам нужен, и если нашли то вернуть
3)если нод не найден то вернуть нулл
 */
        int index = key.hashCode() % size;
        for (Node current = table[index]; current != null; current = current.getNext()) {
            if (current.getKey() == key) {
                return current.getValue();
            }
        }
        return null;
    }

    public void remove(String key) {
        /*
        1  ) найти индекс
        2 )если нашли кей то удалить эту ноду
        2 0)если хеадер== нул то ретурн
        2 1)если наша нода = хэд=> тэйбл индекс = тэйбл индекс нэкст
        2 2)если наша нода = не хэд= предыдуший нэкст=энд нэкст

         */
        int index = key.hashCode() % size;
        if (table[index] == null) {
            return;
        }
        if (table[index].getKey() == key) {
            table[index] = table[index].getNext();
            return;
        }

        System.out.println("xxx: " + table[index]);
        for (Node current = table[index]; current.getNext() != null; current = current.getNext()) {
            if (current.getNext().getKey() == key) {
                current.setNext(current.getNext().getNext());
                return;
            }
        }



    }

    public int getSize() {
        return size;
    }

    public void printAll() {
        for (int i = 0; i < table.length; i++) {
            Node j = table[i];

            System.out.print(i + ": ");
            while (j != null) {
                System.out.print(j.getKey() + "-" + j.getValue() + "\t\t>");
                j = j.getNext();
            }
            System.out.println();
        }
    }
}
