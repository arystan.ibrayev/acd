package kz.aitu.additional.bonus;

import lombok.Data;

@Data
public class Node {
    private String name;
    private Node left;
    private Node right;
    private Node next;

    public Node(String name) {
        this.name = name;
        this.left = null;
        this.right = null;
        this.next = null;
    }

    public boolean addNode(Node next) {
        if(left == null) left = next;
        else if(right == null) right = next;
        else return false;
        return true;
    }
}
