package kz.aitu.additional.bonus;

public class Main {

    //+200points for ADS
    public static void main(String[] args) {
        checkTask();
    }



    public static void checkTask() {
        BonusPoints bonusPoints = new BonusPoints();

        bonusPoints.addRoot("Student1");
        bonusPoints.addChild("Student2", "Student1");
        bonusPoints.addChild("Student3", "Student2");
        bonusPoints.addChild("Student4", "Student3");
        bonusPoints.addChild("Student5", "Student3");
        bonusPoints.addChild("Student6", "Student5");

        System.out.println(bonusPoints.getPoints("Student1")); //returns 93
    }

}
