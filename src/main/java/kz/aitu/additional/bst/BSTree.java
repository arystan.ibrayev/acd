package kz.aitu.additional.bst;

public class BSTree {
    private Node root;

    public BSTree() {
        this.root = null;
    }

    public void insert(Integer key, String value) {
        //WRITE YOUR CODE HERE
        Node node = new Node(key, value);
        if(root == null) {
            root = node;
            return;
        }

        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) {
                if(current.getRight() == null) {
                    current.setRight(node);
                    return;
                }
                current = current.getRight();
            } else if(key < current.getKey()) {
                if(current.getLeft() == null) {
                    current.setLeft(node);
                    return;
                }
                current = current.getLeft();
            }
        }
    }

    public void delete(Integer key) {
        if(root == null) return;if(root.getKey() == key) {
            if(root.getLeft() == null && root.getRight() == null)
                root = null;
        }
        delete(root, key);
    }

    public void delete(Node node, Integer key) {
        Node current = node;
        while(current != null) {
            if(key < current.getKey()) {
                if(current.getLeft() != null && current.getLeft().getKey().equals(key)) {

                    Node left = current.getLeft();
                    if(left.getLeft() == null && left.getRight() == null) {// NO CHILD
                        current.setLeft(null);
                    } else if(left.getLeft() == null || left.getRight() == null) {// ONE CHILD
                        if(left.getLeft() != null) current.setLeft(left.getLeft());
                        else if(left.getRight() != null) current.setLeft(left.getRight());
                    }
                }
                current = current.getLeft();
            } else {
                if(current.getRight() != null && current.getRight().getKey().equals(key)) {
                    Node right = current.getRight();
                    if(right.getLeft() == null && right.getRight() == null) {// NO CHILDREN
                        current.setRight(null);
                    } else if(right.getLeft() == null || right.getRight() == null) {// ONE CHILD
                        if(right.getLeft() != null) current.setRight(right.getLeft());
                        else if(right.getRight() != null) current.setRight(right.getRight());
                    }
                }
                current = current.getRight();
            }
        }

    }

    public Node findMax(Node node){
        Node temp = node;
        while(temp.getRight() != null){
            temp = temp.getRight();
        }
        return temp;
    }

    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending() {
        printNodeAsc(root);
        System.out.println();
    }

    public void printAll() {
    }

    private void printNodeAsc(Node node) {
        if(node == null) return;
        printNodeAsc(node.getLeft());
        System.out.print(node.getValue());
        printNodeAsc(node.getRight());
    }
}
