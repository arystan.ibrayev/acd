package kz.aitu.additional.graph;

import lombok.Data;

import java.util.LinkedList;
import java.util.Objects;

@Data
public class Vertex<Key, Value> {
    private Key key;
    private Value value;
    private LinkedList<Vertex<Key, Value>> edges;

    public Vertex(Key key, Value value) {
        this.key = key;
        this.value = value;
        edges = new LinkedList<>();
    }

    public void addEdge(Vertex vertex) {
        if(!edges.contains(vertex)) edges.add(vertex);
    }

    public boolean equals(Vertex o) {
        if(o == null) return false;
        if(this.key == o.getKey()) return true;
        return false;
    }
}
