package kz.aitu.algo;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    private static final int SUM = 36;

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(4,5,6,7,9,9,9,9,3,1,9);

        list = list.stream().sorted(Comparator.comparingInt(o -> o)).collect(Collectors.toList());

        System.out.println(list);
        System.out.println(isHasSum(list, SUM, 0, list.size() - 1));
    }

    private static boolean isHasSum(List<Integer> list, int sum, int left, int right) {
        int leftTemp = left;
        int rightTemp = right;
        while(leftTemp < rightTemp) {
            int product = list.get(leftTemp) + list.get(rightTemp);
            System.out.println(leftTemp + "-" + rightTemp + " -> " + product);
            if(product == sum) {
                System.out.println(leftTemp + ": " + list.get(leftTemp));
                System.out.println(rightTemp + ": " + list.get(rightTemp));
                return true;
            }
            if(product > sum) rightTemp--;
            else leftTemp++;
        }
        if(left >= right) return false;
        System.out.println("==> " + left + "-" + right + " = " + sum);
        boolean flag = isHasSum(list, sum - list.get(left), left + 1, right);

        if(flag) {
            System.out.println(left + ": " + list.get(left));
        }

        return flag;
    }
}
