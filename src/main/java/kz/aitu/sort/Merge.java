package kz.aitu.sort;

import java.util.Arrays;

public class Merge {

    public static void main(String[] args) {
        Merge merge = new Merge();

        int a[] = {1,3,5,7,9,11,13,31};
        int b[] = {2,4,6,8,9,9, 60};
        int arr[] = merge.merge(a, b);
        printAll(arr);
    }

    public void sort(int[] arr) {

    }

    /*
    * 1 2 3 4 5 6 7 8 9
    * 1 2 3 4       5 6 7 8 9
    * 1 2   3 4     5 6     7 8 9
    * 1 2   3 4     5 6     7 8     9
    *
    *
    */
    public int[] mergeSort(int a[], int b[]) {
        //if(a.length <= 2) sorti();
        //if(b.length <= 2) sorti();


        if(a.length > 2) {
            a = mergeSort(a, a);//half half
        }
        if(b.length > 2) {
            b = mergeSort(b, b);//half half
        }
        return merge(a, b);
    }


    public int[] merge(int[] left, int[] right) {
        int[] result = new int[left.length + right.length];

        int resultIndex = 0;
        int leftIndex = 0;
        int rightIndex = 0;

        while(leftIndex < left.length || rightIndex < right.length) {
            if(leftIndex >= left.length) {
                result[resultIndex++] = right[rightIndex++];
                continue;
            }
            else if(rightIndex >= right.length) {
                result[resultIndex++] = left[leftIndex++];
                continue;
            }

            if(left[leftIndex] < right[rightIndex])
                result[resultIndex++] = left[leftIndex++];
            else
                result[resultIndex++] = right[rightIndex++];
        }
        return result;
    }

    private static void printAll(int[] array) {
        System.out.print("==> ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }
        System.out.println();
    }
}
